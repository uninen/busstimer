const webpack = require('webpack')

const production = process.env.NODE_ENV === 'production'

module.exports = {
  lintOnSave: false,
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({
        DEBUG: !production,
      }),
    ],
  },
}
