// Set ids and lines of FROM and TO stops here.

// If you want to allow all lines, leave the array empty,
// otherwise set which lines to include in the times.

export const fromHomeStop = {
  id: '6208',
  lines: ['801', '901'],
}

export const toHomeStop = {
  id: '1684',
  lines: ['801'],
}

let scheme = 'http'
let apiURL = '://data.foli.fi/siri/sm/'

if (!DEBUG) {
  scheme = 'https'
}

export const fromHomeApiURL = scheme + apiURL + fromHomeStop.id
export const toHomeApiURL = scheme + apiURL + toHomeStop.id
